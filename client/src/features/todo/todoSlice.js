import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

const todosUrl = 'http://localhost:5000/todos';

export const getTodoItems = createAsyncThunk('todo/getTodoItems', () => {
  let todos = fetch(todosUrl)
    .then((res) => res.json())
    .catch((err) => console.log(err));
  return todos; // toastification with the error
});

export const editTodoItem = createAsyncThunk('todo/editTodoItem', (todo) => {
  return axios.put(`${todosUrl}/${todo.id}`, todo).then((resp) => resp.data);
});

export const createNewTodo = createAsyncThunk('todo/createNewTodo', (todo) => {
  return axios.post(`${todosUrl}`, todo).then((resp) => resp.data);
});

export const deleteTodoItem = createAsyncThunk(
  'todo/deleteTodoItem',
  (todoId) => {
    axios
      .delete(`${todosUrl}/${todoId}`)
      .then((resp) => console.log(resp.data));
    return todoId;
  }
);

const todoSlice = createSlice({
  name: 'todo',
  initialState: {
    todos: [],
  },
  extraReducers: {
    [getTodoItems.fulfilled]: (state, action) => {
      state.todos = action.payload;
    },
    [editTodoItem.fulfilled]: (state, action) => {
      console.log(action.payload);
      const item = state.todos.indexOf(
        state.todos.find((item) => item.id === action.payload.id)
      );
      state.todos[item] = action.payload;
    },
    [createNewTodo.fulfilled]: (state, action) => {
      state.todos.push(action.payload);
    },
    [deleteTodoItem.fulfilled]: (state, action) => {
      state.todos.map((todo) => {
        if (todo.id === action.payload)
          state.todos.splice(state.todos.indexOf(todo), 1);
      });
    },
  },
});

export default todoSlice.reducer;
