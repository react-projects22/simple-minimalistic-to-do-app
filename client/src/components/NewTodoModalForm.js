import { Button, Modal, Form, Input, Typography, DatePicker } from 'antd';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { createNewTodo } from '../features/todo/todoSlice';

const { Title } = Typography;

const NewTodoModalForm = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const dispatch = useDispatch();

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const onSubmitForm = (todo) => {
    dispatch(createNewTodo({ ...todo, completed: false }));
    handleCancel();
  };
  return (
    <>
      <Button type='primary' onClick={showModal}>
        New todo
      </Button>
      <Modal
        visible={isModalVisible}
        onCancel={handleCancel}
        footer={[
          <Button key='back' onClick={handleCancel}>
            Return
          </Button>,
        ]}
      >
        <Title level={4}>New todo</Title>
        <Form name='new-todo' onFinish={onSubmitForm} autoComplete='off'>
          <Form.Item
            name='title'
            rules={[
              { required: true, message: 'Please input your todo title!' },
            ]}
          >
            <Input placeholder='Title' />
          </Form.Item>

          <Form.Item
            name='description'
            rules={[
              {
                required: true,
                message: 'Please input your todo description!',
              },
            ]}
          >
            <Input.TextArea placeholder='Description' />
          </Form.Item>
          <Form.Item name='dateCreated'>
            <DatePicker placeholder='Date created' showToday />
          </Form.Item>
          <Form.Item name='dueDate'>
            <DatePicker placeholder='Due date' />
          </Form.Item>
          <Form.Item>
            <Button type='primary' htmlType='submit'>
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default NewTodoModalForm;
