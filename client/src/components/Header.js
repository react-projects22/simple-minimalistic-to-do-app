import { Button, Typography, Space } from 'antd';
import NewTodoModalForm from './NewTodoModalForm';

const { Title } = Typography;
const Header = () => {
  return (
    <header>
      <Title align='center'>Todos</Title>
      <Space style={{ justifyContent: 'center', width: '100%' }}>
        <NewTodoModalForm />
      </Space>
    </header>
  );
};

export default Header;
