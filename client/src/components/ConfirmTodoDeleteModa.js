import { Modal } from 'antd';

const ConfirmTodoDeleteModal = (props) => {
  const { id, title } = props.todo;
  return (
    <Modal
      visible={props.show}
      onOk={props.ok}
      onCancel={props.close}
      title='Deleting item'
    >
      <p>Are you sure you want to delete {title}?</p>
    </Modal>
  );
};

export default ConfirmTodoDeleteModal;
