import { Button, Modal, Form, Input, Typography, DatePicker } from 'antd';
import moment from 'moment';
import { useDispatch } from 'react-redux';
import { editTodoItem } from '../features/todo/todoSlice';
import React from 'react';

const { Title } = Typography;

const EditTodoModalForm = (props) => {
  const dispatch = useDispatch();
  const { id, title, description, completed, dateCreated, dueDate } =
    props.todo;

  let initialValues = {
    title: title,
    description: description,
    dateCreated: moment(dateCreated),
    dueDate: moment(dueDate),
  };

  const onSubmitForm = (todo) => {
    console.log(todo);
    dispatch(editTodoItem({ ...todo, completed: completed, id: id }));
  };

  return (
    <>
      <Modal visible={props.show} onCancel={props.close} footer={[]}>
        <Title level={4}>Edit todo</Title>
        <Form
          name='edit-todo'
          onFinish={() => onSubmitForm(props.todo)}
          autoComplete='off'
          // initialValues={{
          //   title: title,
          //   description: description,
          //   dateCreated: moment(dateCreated),
          //   dueDate: moment(dueDate),
          // }}
        >
          <Form.Item
            name='title'
            rules={[{ required: true, message: 'Please input to-do name!' }]}
          >
            <Input placeholder='Title' value={title} />
          </Form.Item>

          <Form.Item
            name='description'
            rules={[
              { required: true, message: 'Please input to-do description!' },
            ]}
          >
            <Input.TextArea placeholder='Description' />
          </Form.Item>

          <Form.Item name='dateCreated'>
            <DatePicker placeholder='Date created' showToday />
          </Form.Item>

          <Form.Item name='dueDate'>
            <DatePicker placeholder='Due date' />
          </Form.Item>

          <Form.Item>
            <Button type='primary' htmlType='submit'>
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default EditTodoModalForm;
