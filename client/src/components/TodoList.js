import { useSelector } from 'react-redux';
import EditTodoModalForm from './EditTodoModalForm';
import ConfirmTodoDeleteModal from './ConfirmTodoDeleteModa';
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Table, Row, Col } from 'antd';
import {
  CheckCircleOutlined,
  CloseOutlined,
  DeleteOutlined,
  EditOutlined,
} from '@ant-design/icons';

import { editTodoItem, deleteTodoItem } from '../features/todo/todoSlice';
import moment from 'moment';

const TodoList = () => {
  const { todos } = useSelector((store) => store.todos);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [isDeletePopupVisible, setIsDeletePopupVisible] = useState(false);
  const [selectedTodo, setSelectedTodo] = useState({});
  const dispatch = useDispatch();

  const columns = [
    {
      title: 'Title',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: 'Description',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'Date created',
      dataIndex: 'dateCreated',
      key: 'dateCreated',
      render: (_, todo) =>
        todo.dateCreated != null
          ? moment(todo.dateCreated).format('YYYY-MM-DD')
          : null,
    },
    {
      title: 'Due date',
      dataIndex: 'dueDate',
      key: 'dueDate',
      render: (_, todo) =>
        todo.dueDate != null ? moment(todo.dueDate).format('YYYY-MM-DD') : null,
    },
    {
      title: 'Completed',
      align: 'center',
      dataIndex: 'completed',
      key: 'completed',
      render: (_, todo) =>
        todo.completed ? (
          <CloseOutlined
            style={{ color: '#d46b08' }}
            onClick={() =>
              dispatch(
                editTodoItem({
                  ...todo,
                  completed: !todo.completed,
                })
              )
            }
          />
        ) : (
          <CheckCircleOutlined
            style={{
              color: '#389e0d',
            }}
            onClick={() =>
              dispatch(
                editTodoItem({
                  ...todo,
                  completed: !todo.completed,
                })
              )
            }
          />
        ),
    },
    {
      title: 'Actions',
      dataIndex: 'operation',
      align: 'center',
      render: (_, todo) =>
        todos.length >= 1 ? (
          <div>
            <EditTodoModalForm
              show={isEditModalVisible}
              // ok={() =>
              //   dispatch(
              //     editTodoItem({
              //       ...selectedTodo,
              //       completed: selectedTodo.completed,
              //       id: selectedTodo.id,
              //     })
              //   )
              // }
              close={() => {
                console.log(selectedTodo);
                setIsEditModalVisible(false);
              }}
              todo={selectedTodo}
              style={{
                verticalAlign: 'middle',
              }}
            />

            <ConfirmTodoDeleteModal
              show={isDeletePopupVisible}
              ok={() => {
                setIsDeletePopupVisible(false);
                dispatch(deleteTodoItem(selectedTodo.id));
              }}
              close={() => setIsDeletePopupVisible(false)}
              todo={selectedTodo}
            />

            <EditOutlined
              style={{ color: '#1890ff' }}
              onClick={() => {
                setIsEditModalVisible(true);
                setSelectedTodo(todo);
              }}
            />
            <DeleteOutlined
              style={{ color: '#cf1322' }}
              onClick={() => {
                setIsDeletePopupVisible(true);
                setSelectedTodo(todo);
              }}
            />
          </div>
        ) : null,
    },
  ];

  return (
    <div className='space-align-container'>
      <div className='space-align-block' align='center'>
        <Row>
          <Col span={18} offset={3}>
            <Table
              rowKey='id'
              columns={columns}
              dataSource={todos}
              pagination={{
                position: 'bottomRight',
                defaultPageSize: 5,
                showSizeChanger: true,
                pageSizeOptions: ['5', '10', '20'],
              }}
            />
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default TodoList;
