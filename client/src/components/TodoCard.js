import React, { createContext, useState } from 'react';
import { Card, Typography } from 'antd';
import {
  CheckCircleOutlined,
  CloseOutlined,
  DeleteOutlined,
  EditOutlined,
} from '@ant-design/icons';
import { useDispatch } from 'react-redux';
import { editTodoItem, deleteTodoItem } from '../features/todo/todoSlice';
import EditTodoModalForm from './EditTodoModalForm';

const { Text } = Typography;
const TodoCard = (props) => {
  const { id, title, description, completed } = props.todo;
  const [isModalVisible, setIsModalVisible] = useState(false);

  const dispatch = useDispatch();
  return (
    <Card
      title={title}
      actions={[
        completed ? (
          <CloseOutlined
            style={{ color: '#d46b08' }}
            onClick={() =>
              dispatch(
                editTodoItem({
                  id: id,
                  title: title,
                  description: description,
                  completed: !completed,
                })
              )
            }
          />
        ) : (
          <CheckCircleOutlined
            style={{ color: '#389e0d' }}
            onClick={() =>
              dispatch(
                editTodoItem({
                  id: id,
                  title: title,
                  description: description,
                  completed: !completed,
                })
              )
            }
          />
        ),
        <EditOutlined
          style={{ color: '#1890ff' }}
          onClick={() => setIsModalVisible(true)}
        />,
        <DeleteOutlined
          style={{ color: '#cf1322' }}
          onClick={() => dispatch(deleteTodoItem(id))}
        />,
      ]}
    >
      <EditTodoModalForm
        show={isModalVisible}
        close={() => setIsModalVisible(false)}
        todo={props.todo}
      />
      <Text type={completed ? 'success' : 'warning'}>
        {completed ? 'Completed: ' : 'To-do: '}
      </Text>
      <Text type='secondary'>{description}</Text>
    </Card>
  );
};

export default TodoCard;
