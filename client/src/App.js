import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { getTodoItems } from './features/todo/todoSlice';
import TodoList from './components/TodoList';
import Header from './components/Header';
import 'antd/dist/antd.css';

function App() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getTodoItems());
  }, []);
  return (
    <main>
      <Header />
      <br />
      <TodoList />
    </main>
  );
}

export default App;
